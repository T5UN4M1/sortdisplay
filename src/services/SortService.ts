export enum SelectionSortNumberStatus {
    NONE, READ, SWAP, MIN, SORTED
}

class SelectionSorter {

    numbers: number[];
    status: SelectionSortNumberStatus[];

    sorted: number = -1;
    read: number = 0;
    min: number = -1;
    swap: number[] = [-1, -1];

    operations: any = {
        read: 0,
        insert: 0
    }

    constructor(numbers: number[]) {
        this.numbers = [...numbers];
        this.status = [...numbers].map(n => SelectionSortNumberStatus.NONE)
    }

    getReadVal = () => this.numbers[this.read];
    getMinVal = () => this.numbers[this.min];

    addRead = () => ++this.operations.read;
    addInsert = () => ++this.operations.insert;

    next = () => {
        console.log(this.operations.read, this.operations.insert);
        if (this.sorted + 1 >= this.numbers.length) {
            return this.getData();
        }
        if (this.min === -1) {
            this.min = this.read;
            this.addRead();

        } else if (this.swap[0] != -1) {
            ++this.sorted;
            this.read = this.sorted + 1;
            this.min = this.read;
            this.swap = [-1, -1];
            this.addRead();
        } else if (this.read >= this.numbers.length) {
            // swap
            const valsToSwap = [this.numbers[this.sorted + 1], this.numbers[this.min]];

            this.numbers[this.sorted + 1] = valsToSwap[1];
            this.numbers[this.min] = valsToSwap[0];

            this.swap = [this.sorted + 1, this.min];
            this.addInsert();
            return this.getData();
        } else if (this.getReadVal() < this.getMinVal()) {
            this.min = this.read;
            this.addRead();
        }
        ++this.read;
        return this.getData();
    }
    getStatus = (i: number) => {
        if (this.swap.includes(i)) {
            return SelectionSortNumberStatus.SWAP;
        }
        if (i <= this.sorted) {
            return SelectionSortNumberStatus.SORTED;
        }
        if (i === this.min) {
            return SelectionSortNumberStatus.MIN;
        }
        if (i === this.read) {
            return SelectionSortNumberStatus.READ;
        }
        return SelectionSortNumberStatus.NONE;
    }
    getData = () => this.numbers.map((n: number, i: number) => [n, this.getStatus(i)]);

}

export const SortService = {
    getSelectionSorter: (n: number[]) => new SelectionSorter(n)

};