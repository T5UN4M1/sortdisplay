import arrayShuffle from "array-shuffle";


export const NumberService = {
    getNumbers: (start: number, end: number, step: number = 1) => {
        const r = [];
        for (let i = start; i <= end; i += step) {
            r.push(i);
        }
        return r;
    },
    getRandomlySortedNumbers: (amount: number): number[] => arrayShuffle(NumberService.getNumbers(1, amount))

}