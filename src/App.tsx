import React from 'react';
import './App.css';
import {SortDisplay} from "./components/SortDisplay";

function App() {
    return (
        <div className="App">
            <SortDisplay/>
        </div>
    );
}

export default App;
