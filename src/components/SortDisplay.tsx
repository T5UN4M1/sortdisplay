import {Bar, BarChart, Cell, ResponsiveContainer, Tooltip} from "recharts";
import React, {useEffect, useState} from "react";
import {NumberService} from "../services/NumbersService";
import {SelectionSortNumberStatus, SortService} from "../services/SortService";


export const SortDisplay = (props: any) => {
    const toRechartData = (numbers: any[]) => numbers.map(n => ({num: n[0]}));

    const sorter = SortService.getSelectionSorter(NumberService.getRandomlySortedNumbers(30));
    const [data, setData] = useState(sorter.getData());

    const DELAY = 50;

    const CustomTooltip = (props: any) =>
        props.active ? (
            <div className="tooltip">
                Value : {props.payload[0].value}
            </div>
        ) : null;

    const fillColorByStatus = (status: SelectionSortNumberStatus): string => {
        return String(([
            [SelectionSortNumberStatus.NONE, "#fff"],
            [SelectionSortNumberStatus.MIN, "#00f"],
            [SelectionSortNumberStatus.READ, "#f00"],
            [SelectionSortNumberStatus.SORTED, "#ff0"],
            [SelectionSortNumberStatus.SWAP, "#0f0"]
        ].find(entry => entry[0] === status) || ["defaultValue", "#fff"])[1]);
    }

    useEffect(() => {

        const next = () => {
            setData(sorter.next());
            setTimeout(next, DELAY);
        }
        setTimeout(next, DELAY);
    }, [])

    return (
        <ResponsiveContainer width="100%" aspect={2}>
            <BarChart data={toRechartData(data)}>
                <Tooltip content={<CustomTooltip/>}/>
                <Bar dataKey="num" fill="#fff">
                    {
                        data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={fillColorByStatus(entry[1])}/>
                        ))
                    }
                </Bar>
            </BarChart>
        </ResponsiveContainer>
    );
}